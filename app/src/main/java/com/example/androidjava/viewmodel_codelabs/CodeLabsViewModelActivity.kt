package com.example.androidjava.viewmodel_codelabs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.example.androidjava.R

class CodeLabsViewModelActivity : AppCompatActivity() {

    lateinit var viewmodel: GameViewModels

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_model_code_labs)

      viewmodel = ViewModelProviders.of(this).get(GameViewModels::class.java)

    }
}
