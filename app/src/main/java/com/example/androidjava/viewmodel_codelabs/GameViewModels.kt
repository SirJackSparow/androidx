package com.example.androidjava.viewmodel_codelabs

import android.util.Log
import androidx.lifecycle.ViewModel

class GameViewModels  : ViewModel(){

    init {
        Log.i("GameViewModel", "GameViewModel created!")
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("GameViewModel", "GameViewModel destroyed!")
    }
}