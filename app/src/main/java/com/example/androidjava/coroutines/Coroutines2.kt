package com.example.androidjava.coroutines

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidjava.R
import kotlinx.android.synthetic.main.activity_coroutines2.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlin.random.Random
import kotlin.system.measureTimeMillis

class Coroutines2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coroutines2)
        clicked.setOnClickListener {
            fakeAsyncApiRequest()
        }
    }

    private fun fakeAsyncApiRequest() {
        CoroutineScope(IO).launch {
            val executeTimes = measureTimeMillis {

                //async mereturn berdasarkan  type data Defered<String>

                val result1: Deferred<String> = async {
                    println("debug : launching job1: ${Thread.currentThread().name}")
                    getResult1fromApi()
                }

                val result2: Deferred<String> = async {
                    println("debug : launching job2: ${Thread.currentThread().name}")
                    getResult2fromApi()
                }

                var result = ""
                val job = launch {
                    result = getResult1fromApi()
                }
                job.join()
                print(result)

                setTextOnMainThread(result1.await())
                setTextOnMainThread(result2.await())
            }

            println("execute time : $executeTimes")
        }
    }

    private fun fakeApiRequest() {

        val startCurrentTime = System.currentTimeMillis()

        val parentJob = CoroutineScope(IO).launch {
            val job1 = launch {
                val time1 = measureTimeMillis {
                    println("debug Thread1 : ${Thread.currentThread().name}")
                    val result1 = getResult1fromApi()
                    setTextOnMainThread("Got $result1")
                }
                println("debug complete job1 : $time1")
            }
            val job2 = launch {
                val time2 = measureTimeMillis {
                    println("debug Thread2 : ${Thread.currentThread().name}")
                    val result2 = getResult2fromApi()
                    setTextOnMainThread("Got $result2")
                }
                println("debug complete job2: $time2")
            }
        }
        parentJob.invokeOnCompletion {
            println("debug : total times: ${System.currentTimeMillis() - startCurrentTime}")
        }
    }

    private suspend fun setTextOnMainThread(input: String) {
        withContext(Main) {
            setNewText(input)
        }
    }

    private fun setNewText(input: String) {
        val newText = text.text.toString() + "$input\n"
        text.text = newText
    }

    private suspend fun getResult1fromApi(): String {
        delay(1000)
        return "R1"

    }

    private suspend fun getResult2fromApi(): String {
        delay(1700)
        return "R2"
    }


}
