package com.example.androidjava.coroutines

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidjava.R
import kotlinx.android.synthetic.main.activity_coroutines.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class Coroutines : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coroutines)

        coroutines_button.setOnClickListener {
            CoroutineScope(Default).launch {
                FakeInput()
            }
        }

        coba1{ a : Int , b : Int -> a + b}
    }

    private suspend fun setToMainThread(input: String, inpu2: String) = withContext(Main) {
        setText(input,inpu2)
    }




    fun coba1(aku : (Int,Int) -> Int ) {
        aku(2,3)
    }


    private suspend fun FakeInput() {
        setToMainThread(getResult(), helloBima())
    }

    fun setText(input: String,input2:String) {
        val newString = text.text.toString() + input + input2
        text.text = newString
    }

    private suspend fun getResult(): String {
        delay(1000)
        return "Bima"
    }

    private fun helloBima() :  String {
       return "Hello"
    }
}
