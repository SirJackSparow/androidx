package com.example.androidjava.animation.navigationjetpack

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidjava.R

class SharedNavigationAnimation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shared_navigation_animation)
    }
}
