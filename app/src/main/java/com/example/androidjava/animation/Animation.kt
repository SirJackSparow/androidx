package com.example.androidjava.animation

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationSet
import com.example.androidjava.R
import kotlinx.android.synthetic.main.activity_animation.*

class Animation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animation)

        btn.setOnClickListener {
            val animavalue = ValueAnimator.ofFloat(10f, 20f)
            animavalue.duration = 1000
            animavalue.addUpdateListener {
                val animatedValue = it.animatedValue as Float
                btn.textSize = animatedValue

                btn.translationX = 100f
            }
            animavalue.start()
        }

        ObjectAnimator.ofFloat(titles, "translationX", 200f).apply {
            duration = 1000
            start()
        }
        ObjectAnimator.ofFloat(titles, "translationY", 200f).apply {
            duration = 2000
            start()
        }


    }
}
