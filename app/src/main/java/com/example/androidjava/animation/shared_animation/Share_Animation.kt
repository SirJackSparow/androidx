package com.example.androidjava.animation.shared_animation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import com.example.androidjava.R
import kotlinx.android.synthetic.main.activity_share__animation.*

class Share_Animation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share__animation)

        //setting dulu di style android <item name="android:windowContentTransitions">true</item>

        gambar1.setOnClickListener {
            val intent = Intent(this, Detail_Share::class.java)
            val option = ActivityOptionsCompat.makeSceneTransitionAnimation(this, gambar1, ViewCompat.getTransitionName(gambar1)!!)

            startActivity(intent,option.toBundle())
        }

    }
}
