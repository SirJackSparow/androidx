package com.example.androidjava.login;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.example.androidjava.R;
import com.tokopedia.showcase.ShowCaseBuilder;

public class Java_Login_Goole extends AppCompatActivity {

    ShowCaseBuilder showCaseBuilder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java__login__goole);

        showCaseBuilder = new ShowCaseBuilder();
    }

//    public class OreoNotification extends ContextWrapper {
//
//        private static String CHANNEL_ID = "com.example.root.sihmi";
//        private static String CHANNEL_NAME = "sihmi";
//
//        private NotificationManager notificationManager;
//
//        public OreoNotification(Context base) {
//            super(base);
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//                createChannel();
//            }
//        }
//
//        @TargetApi(Build.VERSION_CODES.O)
//        private void createChannel() {
//            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
//            notificationChannel.enableLights(false);
//            notificationChannel.enableVibration(true);
//            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
//
//            getManager().createNotificationChannel(notificationChannel);
//
//        }
//
//        public NotificationManager getManager(){
//            if (notificationManager == null){
//                notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            }
//            return  notificationManager;
//        }
//
//        @TargetApi(Build.VERSION_CODES.O)
//        public Notification.Builder getOreoNotification(String title, String body, PendingIntent pendingIntent, Uri soundUri, String icon){
//            return new Notification.Builder(getApplicationContext(), CHANNEL_ID)
//                    .setContentIntent(pendingIntent)
//                    .setContentTitle(title)
//                    .setContentText(body)
//                    .setSmallIcon(Integer.parseInt(icon))
//                    .setSound(soundUri)
//                    .setAutoCancel(true);
//        }
//    }
//
//    private void sendOreoNotification(RemoteMessage remoteMessage){
//        Log.d(TAG, "sendNotification: Message");
//        int idNotif;
//        Intent intent;
//
//        String user = remoteMessage.getData().get("user");
//        String icon = remoteMessage.getData().get("icon");
//        String title = remoteMessage.getData().get("title");
//        String body = remoteMessage.getData().get("body");
//
//
//        Contact contact = CoreApplication.get().getAppDb().interfaceDao().getContactById(user);
//
//        RemoteMessage.Notification notification = remoteMessage.getNotification();
//        if (title.equals("New Message")){
//            idNotif = 1;
//            intent = new Intent(this, ChatActivity.class)
//                    .putExtra("nama", contact.getFullName()).putExtra("iduser", contact.get_id());
//        } else {
//            idNotif = 2;
//            intent = new Intent(this, ProfilActivity.class);
//        }
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, idNotif, intent, PendingIntent.FLAG_ONE_SHOT);
//        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//        OreoNotification oreoNotification = new OreoNotification(this);
//        Notification.Builder builder = oreoNotification.getOreoNotification(title, body, pendingIntent,
//                defaultSound, icon);
//
//        oreoNotification.getManager().notify(idNotif, builder.build());
//
//    }

}
