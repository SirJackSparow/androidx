package com.example.androidjava.login

import android.content.Intent
import android.os.Bundle
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.androidjava.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_kotlin_login.*


class KotlinLogin : AppCompatActivity() {

    private lateinit var googleSignInOptions: GoogleSignInOptions
    lateinit var firebaseAuth: FirebaseAuth
    lateinit var googleSignInClient: GoogleSignInClient
    private val RC_SIGN_IN = 9001
    lateinit var progressDial: ProgressBar
    lateinit var task: Task<GoogleSignInAccount>
    lateinit var account: GoogleSignInAccount


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin_login)

        // Configure Google Sign In
        googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        progressDial = ProgressBar(this)

        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)

        firebaseAuth = FirebaseAuth.getInstance()


        //set onclick
        gg.setOnClickListener {
            sigIn()
        }

    }

    private fun sigIn() {
        val intent = googleSignInClient.signInIntent
        startActivityForResult(intent, RC_SIGN_IN)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                account = task.getResult(ApiException::class.java)!!
                firebaseAuthWithGoogle(account)
                //get data
                account.displayName
                account.email
                account.familyName
            } catch (e: ApiException) {

            }
        }
    }

    private fun firebaseAuthWithGoogle(acc : GoogleSignInAccount){

        val credebtial =  GoogleAuthProvider.getCredential(acc.idToken,null)
        firebaseAuth.signInWithCredential(credebtial)
                .addOnCompleteListener(this){
                    if (task.isSuccessful){
                        Toast.makeText(applicationContext,acc.displayName,Toast.LENGTH_LONG).show()
                    }else{
                        print("LOL")
                    }
                }
    }


}
