package com.example.androidjava.dynamical_sesuaikan_background_warna_imageview

import android.graphics.Bitmap
import androidx.palette.graphics.Palette
import androidx.palette.graphics.Palette.PaletteAsyncListener
import android.R.attr.bitmap



object PalleteHelper {

    fun cretaePaletteSync(bitmap: Bitmap): Palette {
        val palette = Palette.from(bitmap).generate()
        return palette
    }

    fun createPaletteAsync(bitmap: Bitmap){
        Palette.from(bitmap).generate { palette ->
            val defaultValue = 0x000000
            val vibrant = palette?.getVibrantColor(defaultValue)
            val vibrantLight = palette?.getLightVibrantColor(defaultValue)
            val vibrantDark = palette?.getDarkVibrantColor(defaultValue)
            val muted =  palette?.getMutedColor(defaultValue)
            val mutedLight = palette?.getLightMutedColor(defaultValue)
            val mutedDark = palette?.getDarkMutedColor(defaultValue)


        }
    }
}