package com.example.androidjava.dynamical_sesuaikan_background_warna_imageview

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.palette.graphics.Palette
import com.example.androidjava.R
import kotlinx.android.synthetic.main.activity_dynamical__color__pallete.*


class Dynamical_Color_Pallete : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dynamical__color__pallete)
        implementRadio()
    }

    private fun implementRadio() {
        radio_group.setOnCheckedChangeListener { group, checkedId ->
            val pos = radio_group.indexOfChild(findViewById(checkedId))
            when (pos) {
                0 -> toggleImages(R.drawable.n)
                else -> toggleImages(R.drawable.android)
            }
        }
    }

    private fun toggleImages(drawableIcon: Int) {
        val bitmap = BitmapFactory.decodeResource(resources, drawableIcon)
        main_image_view.setImageBitmap(bitmap)
        //setToolbarColor(bitmap)
        createPaletteAsync(bitmap)
    }

    private fun setToolbarColor(bitmap: Bitmap) {

        val palette = PalleteHelper.cretaePaletteSync(bitmap)
        val vibrantSwatch = checkVibrantSwatch(palette)


    }

    private fun checkVibrantSwatch(palette: Palette): Palette.Swatch? {
        val vibrant = palette.vibrantSwatch
        if (vibrant != null) {
            return vibrant
        }
        return null
    }

    fun createPaletteAsync(bitmap: Bitmap) {
        Palette.from(bitmap).generate { palette ->
            val defaultValue = 0x000000
            val vibrant = palette?.getVibrantColor(defaultValue)
            val vibrantLight = palette?.getLightVibrantColor(defaultValue)
            val vibrantDark = palette?.getDarkVibrantColor(defaultValue)
            val muted = palette?.getMutedColor(defaultValue)
            val mutedLight = palette?.getLightMutedColor(defaultValue)
            val mutedDark = palette?.getDarkMutedColor(defaultValue)

            vibrant?.let {
                vibrantView.setBackgroundColor(it)
            }

            vibrantLight?.let {
                vibrantLightView.setBackgroundColor(it)
            }

            vibrantDark?.let {
                vibrantDarkView.setBackgroundColor(it)
            }

            muted?.let {
                mutedView.setBackgroundColor(it)
            }

            mutedLight?.let {
                mutedLightView.setBackgroundColor(it)
            }

            mutedDark?.let {
                mutedDarkView.setBackgroundColor(it)
            }


        }


    }
}
