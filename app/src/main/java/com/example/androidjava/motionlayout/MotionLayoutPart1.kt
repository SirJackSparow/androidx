package com.example.androidjava.motionlayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidjava.R

class MotionLayoutPart1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_motion_layout_part1)
    }
}