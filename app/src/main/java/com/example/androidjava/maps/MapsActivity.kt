package com.example.androidjava.maps

import android.Manifest
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.androidjava.R
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var menuItem: Menu? = null

    val REQUEST_LOCATION_PERMISSION = 101

    val zoom: Float = 15.0f
    val dbl: Double = 10.0

    var TAG = "Maps"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)


        val mapFragment = SupportMapFragment.newInstance();
        supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, mapFragment).commit();
        mapFragment.getMapAsync(this)
        //jika default menggunakan dibawah
//        <fragment xmlns:android="http://schemas.android.com/apk/res/android"
//        xmlns:map="http://schemas.android.com/apk/res-auto"
//        xmlns:tools="http://schemas.android.com/tools"
//        android:id="@+id/map"
//        android:name="com.google.android.gms.maps.SupportMapFragment"
//        android:layout_width="match_parent"
//        android:layout_height="match_parent"
//        tools:context=".maps.MapsActivity" />
//        val mapFragment = supportFragmentManager
//                .findFragmentById(R.id.map) as SupportMapFragment
//        mapFragment.getMapAsync(this)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap


        //style maps
        try {
            // Customize the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.maps_style))
            if (!success) {
                Log.e(TAG, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e(TAG, "Can't find style. Error: ", e)
        }


        // Add a marker in Sydney and move the camera
        val myHome = LatLng(30.05172, 30.05172)

        //geocoder bayar
        val gcd = Geocoder(this, Locale.getDefault())
        val address = gcd.getFromLocation(myHome.latitude, myHome.longitude, 1)

        val maeker = mMap.addMarker(MarkerOptions()
                .position(myHome)
                .title("geocoder bayar cuy")
                .snippet("${myHome.latitude}, ${myHome.longitude}"))


        //tampikan jendela informasi dalam 3 detik
        CoroutineScope(Dispatchers.Main).launch {
            maeker.showInfoWindow()

            delay(3000)
            maeker.hideInfoWindow()
        }


        //latlng tanpa zoom
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(myHome))

        //latlng dengan zoom
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myHome, zoom))


        //homeoverlay sebagai overlay ketika di perbesar
        val homeoverlay = GroundOverlayOptions().image(BitmapDescriptorFactory.fromResource(R.drawable.android))
                .position(myHome, 100.0f)

        mMap.addGroundOverlay(homeoverlay)

        //ketika diclik lama di coordinat  lain akan muncul marker baru
        setMapLongClick(mMap)

        //menclick  icon restorant, rempat menarik dll supaya bisa muncul informasi
        setClickPoi(mMap)
        enableMyLocatin()

        setInfoWindowClickToPanorama(mMap)
    }

    fun enableMyLocatin() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true
        } else {
            val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
            ActivityCompat.requestPermissions(this, permissions, REQUEST_LOCATION_PERMISSION)
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_LOCATION_PERMISSION -> {
                if (grantResults.isNotEmpty()
                        && grantResults[0]
                        == PackageManager.PERMISSION_GRANTED) {
                    enableMyLocatin()
                }
            }
        }
    }

    fun setMapLongClick(map: GoogleMap) {
        map.setOnMapLongClickListener { latlng ->

            //geocoder bayar
            val gcd = Geocoder(this, Locale.getDefault())
            val address = gcd.getFromLocation(latlng.latitude, latlng.longitude, 1)

            val snippet = String.format(Locale.getDefault(),
                    "Lat: %1$.5f, Long: %2$.5f",
                    latlng.latitude,
                    latlng.latitude)

            map.addMarker(MarkerOptions()
                    .position(latlng)
                    .title("City")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                    .snippet(snippet))
        }


    }


    //poi berisi tempat menarik, restorant , dll . icon akan muncul
    fun setClickPoi(map: GoogleMap) {
        map.setOnPoiClickListener {
            val poimaker = map.addMarker(MarkerOptions().position(it.latLng).title(it.name))
            poimaker.tag = "poi"
            poimaker.showInfoWindow()
        }
    }


    //fun buat type street
    private fun setInfoWindowClickToPanorama(map: GoogleMap) {
        map.setOnInfoWindowClickListener {
            if (it.tag == "poi") {
                val options = StreetViewPanoramaOptions().position(
                        it.position)

                val streetViewFragment = SupportStreetViewPanoramaFragment
                        .newInstance(options)

                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container,
                                streetViewFragment)
                        .addToBackStack(null).commit()

            }

        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.maps_option, menu)
        menuItem = menu
        menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_person_black_24dp)
        return true

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.normal_map -> mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            R.id.satellite_map -> mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
            R.id.hybrid_map -> mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
            R.id.terrain_map -> mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }
}