package com.example.androidjava.notificaation


import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.example.androidjava.R
import kotlinx.android.synthetic.main.activity_notification_standart.*

class NotificationStandart : AppCompatActivity() {

    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: NotificationCompat.Builder
    private val channelId = "com.example.androidjava.notificaation"
    private val desciption = "example"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_notification_standart)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(this, NotificationStandart::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val contentView = RemoteViews(packageName,R.layout.notificationcustom)
        contentView.setTextViewText(R.id.namenotif,"Hay bimbo")

        clicknotif.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationChannel = NotificationChannel(channelId, desciption, NotificationManager.IMPORTANCE_LOW)
                notificationChannel.enableLights(true)
                notificationChannel.lightColor = Color.RED
                notificationChannel.enableVibration(true)
                notificationManager.createNotificationChannel(notificationChannel)
                builder = NotificationCompat.Builder(this, channelId)
                        .setContentTitle("bimbo")
                        .setContentText("fsf")
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.n))
                        .setContentIntent(pendingIntent)

                //custom notif
//                builder = NotificationCompat.Builder(this, channelId)
//                        .setContent(contentView)
//                        .setSmallIcon(R.mipmap.ic_launcher_round)
//                        .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.n))
//                        .setContentIntent(pendingIntent)


            } else {
                builder = NotificationCompat.Builder(this, channelId)
                        .setContentTitle("bimbo")
                        .setContentText("fsf")
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.mipmap.ic_launcher))
                        .setContentIntent(pendingIntent)
            }
            notificationManager.notify(1234,builder.build())
        }
    }
}
