package com.example.androidjava.workmanager.androidviewmodel

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.work.*
import com.example.androidjava.workmanager.IMAGE_MANIPULATION_WORK_NAME
import com.example.androidjava.workmanager.KEY_IMAGE_URI
import com.example.androidjava.workmanager.TAG_OUTPUT
import com.example.androidjava.workmanager.worker.CleanupWorker
import com.example.androidjava.workmanager.worker.SaveImageToFileWorker
import com.example.androidjava.workmanager.worker.WorkManagerBackground

class WorkManagerAndroidViewModel(application: Application) : AndroidViewModel(application) {

    //    OneTimeWorkRequest:A WorkRequestyang hanya akan dieksekusi satu kali.
    //    PeriodicWorkRequest:A WorkRequestyang akan berulang pada suatu siklus.

    internal var imageUri: Uri? = null
    internal var outputUri: Uri? = null
    val workManager = WorkManager.getInstance(application)
    internal val outputWorkInfos: LiveData<List<WorkInfo>>

    init {
        // This transformation makes sure that whenever the current work Id changes the WorkInfo
        // the UI is listening to changes
        //biar selalu update, untuk melihat tag , lihat val save di worker bawah, untuk melihat tag output
        outputWorkInfos = workManager.getWorkInfosByTagLiveData(TAG_OUTPUT)
    }

    internal fun cancelWork() {
        workManager.cancelUniqueWork(IMAGE_MANIPULATION_WORK_NAME)
    }

    /**
     * Creates the input data bundle which includes the Uri to operate on
     * @return Data which contains the Image Uri as a String
     */

    //input data ke workmanager
    private fun createInputDataForUri(): Data {
        val builder = Data.Builder()
        imageUri?.let {
            builder.putString(KEY_IMAGE_URI, imageUri.toString())
        }
        return builder.build()
    }


    //penghubung ke wormanagerbackground
    internal fun applyBlur(blurLevel: Int) {
        //panggil workmanagerbackground

        // jika tidak ada data untuk dilempar
        //  workManager.enqueue(OneTimeWorkRequest.from(WorkManagerBackground::class.java))

        //tunggal
        //val blur = OneTimeWorkRequestBuilder<WorkManagerBackground>()
        //                .setInputData(createInputDataForUri())
        //                .build()

        //workManager.enqueue(blur)


        //tambah workmanager bersarang
        var continuation = workManager.beginWith(OneTimeWorkRequest
                .from(CleanupWorker::class.java))

        val blurRequest = OneTimeWorkRequest.Builder(WorkManagerBackground::class.java)
                .setInputData(createInputDataForUri())
                .build()

        continuation = continuation.then(blurRequest)

        // Add WorkRequest to save the image to the filesystem
        val save = OneTimeWorkRequest.Builder(SaveImageToFileWorker::class.java)
                .addTag(TAG_OUTPUT)
                .build()

        continuation = continuation.then(save)

        // Actually start the work
        continuation.enqueue()


    }


    private fun uriOrNull(uriString: String?): Uri? {
        return if (!uriString.isNullOrEmpty()) {
            Uri.parse(uriString)
        } else {
            null
        }
    }

    /**
     * Setters
     */
    internal fun setImageUri(uri: String?) {
        imageUri = uriOrNull(uri)
    }

    internal fun setOutputUri(outputImageUri: String?) {
        outputUri = uriOrNull(outputImageUri)
    }
}