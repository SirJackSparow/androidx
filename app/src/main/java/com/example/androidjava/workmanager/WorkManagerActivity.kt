package com.example.androidjava.workmanager


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.work.WorkInfo
import com.bumptech.glide.Glide
import com.example.androidjava.R
import com.example.androidjava.workmanager.androidviewmodel.WorkManagerAndroidViewModel
import kotlinx.android.synthetic.main.activity_work_manager.*


class WorkManagerActivity : AppCompatActivity() {

    lateinit var viewModel : WorkManagerAndroidViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_manager)

         viewModel = ViewModelProvider(this)
                .get(WorkManagerAndroidViewModel::class.java)

        //mengambil data url dari intent
        val imageUriextra = intent.getStringExtra(KEY_IMAGE_URI)

        viewModel.setImageUri(imageUriextra)

        viewModel.imageUri?.let { imageUri ->
            Glide.with(this).load(imageUri).into(image_view)
        }

        go_button.setOnClickListener {
            viewModel.applyBlur(8)
        }

        //lihat file output
        see_file_button.setOnClickListener {
            viewModel.outputUri?.let { currentUri ->
                val actionView = Intent(Intent.ACTION_VIEW, currentUri)
                actionView.resolveActivity(packageManager)?.run {
                    startActivity(actionView)
                }
            }
        }


        //cancel tombol
        cancel_button.setOnClickListener { viewModel.cancelWork() }

        viewModel.outputWorkInfos.observe(this, workInfoObserver())
    }

    private fun workInfoObserver (): Observer<List<WorkInfo>>{
        return Observer { listOfWorkInfo ->

            // Note that these next few lines grab a single WorkInfo if it exists
            // This code could be in a Transformation in the ViewModel; they are included here
            // so that the entire process of displaying a WorkInfo is in one location.

            // If there are no matching work info, do nothing
            if (listOfWorkInfo.isNullOrEmpty()) {
                return@Observer
            }

            // We only care about the one output status.
            // Every continuation has only one worker tagged TAG_OUTPUT
            val workInfo = listOfWorkInfo[0]

            if (workInfo.state.isFinished) {
               Toast.makeText(this,"Finishe", Toast.LENGTH_SHORT).show()

                // Normally this processing, which is not directly related to drawing views on
                // screen would be in the ViewModel. For simplicity we are keeping it here.
                val outputImageUri = workInfo.outputData.getString(KEY_IMAGE_URI)

                // If there is an output file show "See File" button
                if (!outputImageUri.isNullOrEmpty()) {
                    viewModel.setOutputUri(outputImageUri as String)
                    see_file_button.visibility = View.VISIBLE
                }
            } else {
                Toast.makeText(this,"In Progress ....", Toast.LENGTH_SHORT).show()

            }
        }
    }
}