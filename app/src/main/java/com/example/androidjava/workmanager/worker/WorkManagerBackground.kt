package com.example.androidjava.workmanager.worker

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import android.text.TextUtils
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.androidjava.workmanager.*

class WorkManagerBackground(context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {

    override fun doWork(): Result {

        //Anda akan membutuhkan ini untuk berbagai manipulasi bitmap yang akan Anda lakukan.
        val appContext = applicationContext

        makeStatusNotification("Blurring image", appContext)

        //menerima inputan data
        val resource = inputData.getString(KEY_IMAGE_URI)

        //memberi waktu
        sleep()

        return try {
            //jika tidak lempar data
//            val picture = BitmapFactory
//                    .decodeResource(appContext.resources, R.drawable.android)

            //check resource kosong atau tidak
            if (TextUtils.isEmpty(resource)) {
                throw IllegalArgumentException("Invalid input uri")
            }

            //jika ada lempar data
            val resolver = appContext.contentResolver
            val picture = BitmapFactory.decodeStream(
                    resolver.openInputStream(Uri.parse(resource))
            )

            val output = blurBitmap(picture, appContext)

            // Write bitmap to a temp file
            val outputUri = writeBitmapToFile(appContext, output)

            val outputData = workDataOf(KEY_IMAGE_URI to outputUri.toString())

            //jika berhasil
            Result.success(outputData)

        } catch (throwable: Throwable) {

            //jika gagal
            Result.failure()
        }
    }


}