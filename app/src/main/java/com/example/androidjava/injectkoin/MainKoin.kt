package com.example.androidjava.injectkoin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidjava.R

class MainKoin : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_koin)
    }
}
