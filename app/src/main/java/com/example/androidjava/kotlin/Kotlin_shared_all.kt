package com.example.androidjava.kotlin

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.view.MenuItemCompat.getActionProvider
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.ShareActionProvider
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.example.androidjava.R
import com.onesignal.OneSignal
import kotlinx.android.synthetic.main.activity_kotlin_shared_all.*
import kotlinx.android.synthetic.main.activity_sharing.*
import kotlinx.android.synthetic.main.activity_sharing.shareds

class Kotlin_shared_all : AppCompatActivity() {

    var token : String = ""


    private  var shareActionprovider:ShareActionProvider? = null
    var nama: String? = null
    private lateinit var menuItem: MenuItem
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin_shared_all)

        shareds.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_TEXT,"yo")
            intent.type = "text/*"
            startActivity(Intent.createChooser(intent,"Share With"))

        }

        copying.setOnClickListener{
            val clibboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("token",title)
            clibboard.primaryClip = clip
        }




    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //kurang as? jika terjadi kesalahan null cannot be cast to non-null type android.support.v7.widget.ShareActionProvider
        menuInflater.inflate(R.menu.shareds,menu)
        menuItem =  menu?.findItem(R.id.menu_share)!!
        shareActionprovider = (getActionProvider(menuItem) as? ShareActionProvider)
        return true

    }


}
