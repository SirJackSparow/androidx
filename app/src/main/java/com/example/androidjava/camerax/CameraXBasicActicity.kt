package com.example.androidjava.camerax

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.extensions.BokehImageCaptureExtender
import androidx.camera.extensions.HdrImageCaptureExtender
import androidx.camera.extensions.NightImageCaptureExtender
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.androidjava.R
import kotlinx.android.synthetic.main.activity_camera_x_acticity.*
import java.io.File
import java.nio.ByteBuffer
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

typealias LumaListener = (luma: Double) -> Unit

class CameraXActicity : AppCompatActivity() {

    private var preview: Preview? = null
    private var imageCapture: ImageCapture? = null
    private var imageAnalyzer: ImageAnalysis? = null
    private var camera: Camera? = null



    private lateinit var outputDirectory: File
    private lateinit var cameraExecutor: ExecutorService


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera_x_acticity)

        //Periksa apakah kode permintaan sudah true; abaikan saja jika tidak.
        if (allPermissionsGranted()) {
            startCamera()

        } else {
            ActivityCompat.requestPermissions(
                    this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }

        // Setup the listener for take photo button
        camera_capture_button.setOnClickListener { takePhoto() }

        outputDirectory = getOutputDirectory()

        cameraExecutor = Executors.newSingleThreadExecutor()
    }


    private fun startCamera() {
        //Ini digunakan untuk mengikat siklus hidup kamera ke pemilik siklus hidup.
        // Ini memungkinkan Anda untuk tidak khawatir tentang membuka dan menutup kameraƒ
        // karena CameraX sadar akan siklus hidup.
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)

        cameraProviderFuture.addListener(Runnable {
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            //preview
            preview = Preview.Builder()
                    .build()

            //image capture
            imageCapture = ImageCapture.Builder()
                    //flash on
                    .setFlashMode(ImageCapture.FLASH_MODE_OFF)
                    .setCaptureMode(ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY)
                    .build()

            //imageAnalyzer
            imageAnalyzer = ImageAnalysis.Builder()
                    .setTargetResolution(Size(1280, 1720))
                    .build().also {
                        it.setAnalyzer(cameraExecutor, LuminosityAnalyzer { luma ->
                            Log.d(TAG, "Average luminosity: $luma")
                        })
                    }

            // Select back camera
            val cameraSelector = CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build()


            //effect bokeh
            val builder = ImageCapture.Builder()
            val bokehImageCapture = BokehImageCaptureExtender.create(builder)
            val hdrImageCaptureExtender = HdrImageCaptureExtender.create(builder)
            val nightImageCaptureExtender = NightImageCaptureExtender.create(builder)

            when {
                bokehImageCapture.isExtensionAvailable(cameraSelector) -> {
                    // Enable the extension if available.
                    bokehImageCapture.enableExtension(cameraSelector)
                    Toast.makeText(this, "bokeh", Toast.LENGTH_SHORT).show()

                }
                hdrImageCaptureExtender.isExtensionAvailable(cameraSelector) -> {
                    hdrImageCaptureExtender.enableExtension(cameraSelector)
                    Toast.makeText(this, "hdr", Toast.LENGTH_SHORT).show()
                }
                nightImageCaptureExtender.isExtensionAvailable(cameraSelector) -> {
                    Toast.makeText(this, "malam", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    //bokehImageCapture.enableExtension(cameraSelector)
                    Toast.makeText(this, "tak ada", Toast.LENGTH_SHORT).show()

                }
            }


            try {
                cameraProvider.unbindAll()
                
                camera = cameraProvider.bindToLifecycle(this, cameraSelector,
                        preview, imageCapture, imageAnalyzer)

                preview?.setSurfaceProvider(viewFinder.createSurfaceProvider())

            } catch (exc: Exception) {
                Log.e(TAG, "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(this))
    }

    private fun takePhoto() {

        val imageCapture = imageCapture ?: return

        //buat file untuk menahan gambar. Tambahkan cap waktu sehingga nama file akan unik.
        val photoFile = File(
                outputDirectory,
                SimpleDateFormat(FILENAME_FORMAT, Locale.US
                ).format(System.currentTimeMillis()) + ".jpg")

        //dapat menentukan hal-hal tentang bagaimana Anda ingin hasil Anda.
        // Anda ingin output disimpan dalam file yang baru saja kami buat, jadi tambahkan file Anda photoFile.
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        //pelaksana, dan panggilan balik untuk saat gambar disimpan. Anda akan mengisi panggilan balik berikutnya.
        imageCapture.takePicture(

                outputOptions, ContextCompat.getMainExecutor(this), object :
                ImageCapture.OnImageSavedCallback {


            //jika gagal
            override fun onError(exc: ImageCaptureException) {
                Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
            }

            //Jika pengambilan tidak gagal, foto diambil dengan sukses! Simpan foto ke file yang Anda buat sebelumnya,
            // berikan bersulang untuk membiarkan pengguna tahu itu berhasil, dan cetak pernyataan log.
            override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                val savedUri = Uri.fromFile(photoFile)
                val msg = "Photo capture succeeded: $savedUri"

                viewFinder.visibility = View.GONE
                images.setImageURI(savedUri)

                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                Log.d(TAG, msg)
            }
        })

    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
                baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    fun getOutputDirectory(): File {
        val mediaDir = externalMediaDirs.firstOrNull()?.let {
            File(it, resources.getString(R.string.app_name)).apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else filesDir
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {

        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera()
            } else {
                //jika izin tidak diberikan,
                Toast.makeText(this, "Permissions not granted by the user.",
                        Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    companion object {
        private const val TAG = "CameraXBasic"
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }


    private class LuminosityAnalyzer(private val listener: LumaListener) : ImageAnalysis.Analyzer {

        private fun ByteBuffer.toByteArray(): ByteArray {
            rewind()    // Rewind the buffer to zero
            val data = ByteArray(remaining())
            get(data)   // Copy the buffer into a byte array
            return data // Return the byte array
        }

        override fun analyze(image: ImageProxy) {

            val buffer = image.planes[0].buffer
            val data = buffer.toByteArray()
            val pixels = data.map { it.toInt() and 0xFF }
            val luma = pixels.average()

            listener(luma)

            image.close()
        }
    }
}