package com.example.androidjava.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.androidjava.R
import kotlinx.android.synthetic.main.activity_broadcast.*

class BroadcastActivity : AppCompatActivity() {

    lateinit var receiver: BroadcastReceiver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_broadcast)

        //broadcast charger colok baterray or tidak
        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_POWER_CONNECTED)
        filter.addAction(Intent.ACTION_POWER_DISCONNECTED)

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {

                if (intent?.action == Intent.ACTION_POWER_CONNECTED) {
                    Toast.makeText(this@BroadcastActivity, "Charge", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this@BroadcastActivity, "UnCharge", Toast.LENGTH_SHORT).show()
                }


            }
        }
        registerReceiver(receiver, filter)


        //gunakan MyReceiver class berbeda dengan  atas
        broadcast.setOnClickListener {
            sendBroadcast(Intent(this, MyReceiver::class.java))
        }
    }

    override fun onDestroy() {
        unregisterReceiver(receiver)
        super.onDestroy()

    }
}