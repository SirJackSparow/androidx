package com.example.androidjava.firebasefirestore

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataModel(var id: String?, var title: String?, var content: String?) : Parcelable {

    fun toMap(): HashMap<String, Any?> {
        val result: HashMap<String, Any?> = HashMap()
        result["id"] = this.id
        result["title"] = this.title
        result["content"] = this.content
        return result
    }
}

