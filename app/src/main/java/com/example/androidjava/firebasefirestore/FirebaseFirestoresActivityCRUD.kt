package com.example.androidjava.firebasefirestore

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidjava.R
import com.google.firebase.firestore.*
import kotlinx.android.synthetic.main.activity_firebase_firestores_c_r_u_d.*


class FirebaseFirestoresActivityCRUD : AppCompatActivity() {

    val firebaseFireStore = FirebaseFirestore.getInstance()
    lateinit var adapters: AdapterNote
    lateinit var firestoreListener: ListenerRegistration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase_firestores_c_r_u_d)

        //loadNoteList()
        firestoreListener = firebaseFireStore.collection("RnDCRUD")
                .orderBy("id", Query.Direction.DESCENDING)
                .limit(5)
                .addSnapshotListener { value, error ->
                    val datas: MutableList<DataModel> = mutableListOf()
                    if (error != null) {
                        return@addSnapshotListener
                    }

                    if (value != null) {
                        for (x in value) {

                            datas.add(DataModel(
                                    id = x.id,
                                    title = x.data["title"] as String?,
                                    content = x.data["content"] as String?
                            ))
                        }
                    }

                    adapters = AdapterNote(datas) {
                        val intent = Intent(this, Note_Activity::class.java)
                        intent.putExtra("datas", it)

                        startActivity(intent)
                    }

                    rvNoteList.apply {
                        layoutManager = LinearLayoutManager(
                                this@FirebaseFirestoresActivityCRUD)
                        adapter = adapters
                    }
                }
    }

    fun loadNoteList() {

        val datas: MutableList<DataModel> = mutableListOf()
        firebaseFireStore.collection("RnDCRUD")
                .get().addOnCompleteListener {
                    if (it.isSuccessful) {
                        for (x in it.result!!) {

                            datas.add(DataModel(
                                    id = x.id,
                                    title = x.data["title"] as String?,
                                    content = x.data["content"] as String?
                            ))
                        }
                        adapters = AdapterNote(datas) {

                        }

                        rvNoteList.apply {
                            layoutManager = LinearLayoutManager(
                                    this@FirebaseFirestoresActivityCRUD)
                            adapter = adapters
                        }
                    }
                }

    }

    override fun onDestroy() {
        super.onDestroy()
        firestoreListener.remove()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.addNote -> {
                startActivity(Intent(this, Note_Activity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }
}