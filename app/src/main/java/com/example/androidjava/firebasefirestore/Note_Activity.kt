package com.example.androidjava.firebasefirestore

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.androidjava.R
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_note_.*
import java.lang.NullPointerException

class Note_Activity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_)

        val datax: DataModel? = intent.getParcelableExtra("datas")

        try {
            edtTitle.setText(datax!!.title)
            edtContent.setText(datax.content)
        } catch (e: NullPointerException) {
            Snackbar.make(ll, "Tambah Data", Snackbar.LENGTH_LONG).show()
        }

        btAdd.setOnClickListener {
            if (datax != null) {
                update(datax.id, edtTitle.text.toString(), edtContent.text.toString())
            } else {

                addData(edtTitle.text.toString(), edtContent.text.toString())
            }
        }
    }


    fun addData(title: String, content: String) {
        val id: String = db.collection("RnDCRUD").document().id
        val note: HashMap<String, Any?> = DataModel(id = id, title = title,
                content = content).toMap()
        db.collection("RnDCRUD")
                .add(note)
                .addOnSuccessListener {
                    Snackbar.make(ll, "Success", Snackbar.LENGTH_LONG).show()
                    startActivity(Intent(this,
                            FirebaseFirestoresActivityCRUD::class.java))
                }
    }

    fun update(id: String?, title: String, content: String) {
        val note: HashMap<String, Any?> = DataModel(id = id,
                title = title, content = content).toMap()
        db.collection("RnDCRUD")
                .document(id!!)
                .set(note)
                .addOnSuccessListener {
                    Snackbar.make(ll, "Success", Snackbar.LENGTH_LONG).show()
                    startActivity(Intent(this,
                            FirebaseFirestoresActivityCRUD::class.java))
                }.addOnFailureListener {
                    Snackbar.make(ll, "Failure", Snackbar.LENGTH_LONG).show()
                }
    }


}