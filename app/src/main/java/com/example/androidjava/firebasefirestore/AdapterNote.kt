package com.example.androidjava.firebasefirestore

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidjava.R
import com.google.firebase.firestore.FirebaseFirestore

class AdapterNote(val data: MutableList<DataModel>, val listener: (DataModel) -> Unit) :
        RecyclerView.Adapter<ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout
                .item_firebasefirestore, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val datax = data[position]

        holder.titleTv.text = datax.title
        holder.tvContent.text = datax.content

        holder.ivEdit.setOnClickListener {
            listener(DataModel(datax.id, datax.title, datax.content))
        }

        holder.ivDelete.setOnClickListener {
            deleteNote(datax.id!!, position)
        }
    }
}

fun deleteNote(id: String, position: Int) {
    val db = FirebaseFirestore.getInstance()
    Log.e("FAG", id)
    db.collection("RnDCRUD").document(id).delete().addOnSuccessListener {
        Log.e("FAG", id)
    }
}


class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val titleTv: TextView = itemView.findViewById(R.id.tvTitle)
    val tvContent: TextView = itemView.findViewById(R.id.tvContent)
    val ivEdit: ImageView = itemView.findViewById(R.id.ivEdit)
    val ivDelete: ImageView = itemView.findViewById(R.id.ivDelete)

}