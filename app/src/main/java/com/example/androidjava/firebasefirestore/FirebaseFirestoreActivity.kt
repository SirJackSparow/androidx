package com.example.androidjava.firebasefirestore

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.androidjava.R
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_firebase_firestore.*

class FirebaseFirestoreActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase_firestore)


        val db = FirebaseFirestore.getInstance()

        //ketika document di create dengan auto generate jadi tidak operlu tulis collection.document()
        db.collection("RnD").get().addOnSuccessListener { doc ->
            for (x in doc) {
                Log.e("TAG", "${x.data["nama"]}")
            }
        }.addOnFailureListener { exception ->
            Log.e("TAG", "$exception")
        }


        val dataB = hashMapOf("id" to 2, "nama" to "Rezeki")
        add.setOnClickListener {
            // set data biasa
            db.collection("RnD").document("datadummy").set(dataB).addOnSuccessListener {
                Snackbar.make(constrainLayout, "Success", Snackbar.LENGTH_LONG).show()
            }.addOnFailureListener {
                Snackbar.make(constrainLayout, "Failed", Snackbar.LENGTH_LONG).show()
            }
        }
    }
}