package com.example.androidjava.biometric

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.example.androidjava.R
import java.util.concurrent.Executor

class Biometric : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_biometric)

        val executor = ContextCompat.getMainExecutor(this)
        val biometricManager = BiometricManager.from(this)

        //check apakah hp support biometric ini bisa dipakai atau tidak
        when (biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS ->
                authUser(executor)
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                authUser(executor)
//                Toast.makeText(this,
//                        getString(R.string.error_msg_no_biometric_hardware),
//                        Toast.LENGTH_LONG
//                ).show()
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                Toast.makeText(this,
                        getString(R.string.error_msg_biometric_hw_unavailable),
                        Toast.LENGTH_LONG
                ).show()
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED ->
                Toast.makeText(this,
                        getString(R.string.error_msg_biometric_not_setup),
                        Toast.LENGTH_LONG
                ).show()
        }

    }

    private fun authUser(executor: Executor) {
        //untuk membangun dialog otentikasi.
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
                // judul dialog
                .setTitle(getString(R.string.auth_title))
                // subtitle dialog
                .setSubtitle(getString(R.string.auth_subtitle))
                // description
                .setDescription(getString(R.string.auth_description))
                // jika tidak mendukung sidik jari otomatis ganti dengan pola
                .setDeviceCredentialAllowed(true)
                // build
                .build()

        // penanganan ketika success , error dan failed
        val biometricPrompt = BiometricPrompt(this, executor, object : BiometricPrompt.AuthenticationCallback() {
            // 2
            override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
            ) {
                super.onAuthenticationSucceeded(result)
                Toast.makeText(
                        applicationContext,
                        getString(R.string.success_dialog),
                        Toast.LENGTH_SHORT
                ).show()
            }

            // 3
            override fun onAuthenticationError(
                    errorCode: Int, errString: CharSequence
            ) {
                super.onAuthenticationError(errorCode, errString)
                Toast.makeText(
                        applicationContext,
                        getString(R.string.error_msg_auth_error, errString),
                        Toast.LENGTH_SHORT
                ).show()
            }

            // 4
            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                Toast.makeText(applicationContext,
                        getString(R.string.error_msg_auth_failed),
                        Toast.LENGTH_SHORT
                ).show()
            }
        })


        biometricPrompt.authenticate(promptInfo)
    }

}