package com.example.androidjava.notif_device_to_device

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ModelSender(
        @SerializedName("to")
        @Expose
        val to : String,
        @SerializedName("data")
        @Expose
        val notification : NotifData,
        @SerializedName("message_id")
        @Expose
        val message_id: String
        )