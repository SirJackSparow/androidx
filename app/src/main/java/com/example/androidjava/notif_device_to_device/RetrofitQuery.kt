package com.example.androidjava.notif_device_to_device


import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.Call
import retrofit2.Response


interface RetrofitQuery {
    @Headers(
            "Authorization: " + NotifBetweenDeviceActivity.serverKey,
            "Content-Type: application/json"
    )
    @POST("fcm/send")
    fun pushNotification(@Body jsonObject: JSONObject)
            : Call<JSONObject>

    @Headers(
            "Authorization: " + NotifBetweenDeviceActivity.serverKey,
            "Content-Type: application/json"
    )
    @POST("fcm/send")
    fun pushNotif(@Body data: ModelSender) :  Call<ResponseBody>

}