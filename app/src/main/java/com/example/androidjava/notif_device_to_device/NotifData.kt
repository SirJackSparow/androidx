package com.example.androidjava.notif_device_to_device

data class NotifData (
        val title :  String,
        val message :  String
)