package com.example.androidjava.notif_device_to_device

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ServerRetrofit {
    companion object {
        fun getClientData(): RetrofitQuery {

            val retrofit3 = Retrofit.Builder()
                    .baseUrl(NotifBetweenDeviceActivity.FCM_API)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(RetrofitQuery::class.java)

            return retrofit3
        }
    }
}