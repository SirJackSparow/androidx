package com.example.androidjava.notif_device_to_device

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.androidjava.R
import com.example.androidjava.notif_device_to_device.ServerRetrofit.Companion.getClientData
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging


import kotlinx.android.synthetic.main.activity_notif_between_device.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NotifBetweenDeviceActivity : AppCompatActivity() {
    companion object {
        const val FCM_API = "https://fcm.googleapis.com/"
        const val serverKey =
                "key=" + "AAAAP4QZV8Y:APA91bF2K5j2f0GYWgwGDsXm1YcgoAT95GJo4R6sonvEgmP4m3EnAX2qH4kxoK7gJfeukGYjvDcV6pwsDuiVIp7f-ne8aaKY7SU_hvUPnPj1vcFHIJk6acaaQ3JOXG1xs65efEDm-q75"


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notif_between_device)

        FirebaseMessaging.getInstance().subscribeToTopic("/topics/cotabeem")

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Log.e("TAG", task.result?.token)
            }
        }

        btn.setOnClickListener {

            val topic = "/topics/cotabeem" //topic has to match what the receiver subscribed to

            val notifData = NotifData("Hello", "Selamat Pagi")

            getClientData().pushNotif(ModelSender(topic, notifData, "gasjdagdg")).enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    Log.e("TAG", response.code().toString())
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.e("TAG", t.message)
                }

            })

        }

    }
}