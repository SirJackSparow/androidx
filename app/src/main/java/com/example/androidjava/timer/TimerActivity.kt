package com.example.androidjava.timer

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.androidjava.R
import kotlinx.android.synthetic.main.timer.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class TimerActivity : AppCompatActivity() {

    var diff: Long = 0
    var oldLong: Long = 0
    var NewLong: Long = 0
    lateinit var oldDate : Date
    lateinit var newDate :  Date



    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.timer)

        val formatter = SimpleDateFormat("dd.MM.yyyy, HH:mm:ss")
        val oldTime = "26.11.2019, 12:00:00"
        val newTime = "26.11.2019, 12:11:00"

        try {
            oldDate =  formatter.parse(oldTime)
            newDate = formatter.parse(newTime)
            oldLong = oldDate.time
            NewLong =  newDate.time
            diff = NewLong - oldLong
        }catch (e : ParseException){
            e.printStackTrace()
        }

        TimerHelper.TimerHelper(diff,1000,timer).start()

    }

}
