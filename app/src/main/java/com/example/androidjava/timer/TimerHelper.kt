package com.example.androidjava.timer

import android.os.CountDownTimer
import android.widget.TextView
import java.util.concurrent.TimeUnit


object TimerHelper {
    class TimerHelper(millisInFuture: Long, countDownInterval: Long,val timers: TextView) : CountDownTimer(millisInFuture, countDownInterval) {
        override fun onFinish() {
            timers.text = "Selesai"
        }

        override fun onTick(millisUntilFinished: Long) {
            val millis = millisUntilFinished

            val hms = (TimeUnit.MILLISECONDS.toDays(millis).toString() + "Day "
                    + ((TimeUnit.MILLISECONDS.toHours(millis/1000) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis))).toString() +  ":")
                    + ((TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis))).toString() + ":"
                    + (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))))

            val hours = (TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis))).toString()

            val minutesr =  if(((TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis))).toString()).length < 2 )
               "0"+(TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis))).toString()
                 else (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis))).toString()



           val secon = if(((TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))).toString()).length < 2 )
               "0"+(TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))).toString()
           else (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))).toString()

            timers.text = "$hours : $minutesr : $secon"
        }
    }
}


