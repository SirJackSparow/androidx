package com.example.androidjava.recyclerview_diffutils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.androidjava.R

class RatingAdapter : RecyclerView.Adapter<RatingAdapter.ViewHolder>() {

    val datas = ArrayList<Data_Rating>()

    private lateinit var del: Deletes

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_diffutils_recyclerview, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = datas.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = datas[position]

        holder.judul.text = data.judul
        holder.message.text = data.message

        holder.itemView.setOnClickListener {
            del.onItemClick(position)
        }

    }

    fun onClickItemDelete(del: Deletes) {
        this.del = del
    }

    fun setData(Newdata: List<Data_Rating>) {
        val diffCallback = RatingDiffCallback(datas, Newdata)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        //datas.clear()
        datas.addAll(Newdata)
        diffResult.dispatchUpdatesTo(this)

    }

    fun deleteData(position: Int, Newdata: List<Data_Rating>) {
        val diffCallback = RatingDiffCallback(datas, Newdata)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        datas.removeAt(position)
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateData(position: Int, data: String) {
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val judul: TextView = itemView.findViewById(R.id.title)
        val message: TextView = itemView.findViewById(R.id.message)


    }
}

interface Deletes {
    fun onItemClick(position: Int)
}