package com.example.androidjava.recyclerview_diffutils

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidjava.R
import kotlinx.android.synthetic.main.activity_recycler__diff_utils.*

class Recycler_DiffUtils : AppCompatActivity() {

    lateinit var adapters: RatingAdapter
    val dataRatings = ArrayList<Data_Rating>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler__diff_utils)
        val linearLayoutManager = LinearLayoutManager(this)
        adapters = RatingAdapter()

        data.apply {
            layoutManager = linearLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = adapters
            adapters.setData(DataSource.listData)
        }

        data.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                Log.e("TAG", dy.toString())
                Log.e("TAG", "g: "+dx.toString())
                super.onScrolled(recyclerView, dx, dy)

            }
        })


        add.setOnClickListener {
            dataRatings.add(Data_Rating("jikso", "demons", 12, 1))
            adapters.setData(dataRatings)
            adapters.notifyDataSetChanged()
        }


        adapters.onClickItemDelete(object : Deletes {
            override fun onItemClick(position: Int) {
                dataRatings.clear()
                adapters.deleteData(position, DataSource.listData)
                adapters.notifyDataSetChanged()
                Log.e("Bima", adapters.datas.size.toString())
            }

        })


        Log.e("Bima", adapters.datas[1].judul)


    }
}
