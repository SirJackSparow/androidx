package com.example.androidjava.recyclerview_diffutils

object DataSource {

    val listData: List<Data_Rating>
        get() {
            val dataRatings = ArrayList<Data_Rating>()
            dataRatings.add(Data_Rating("Marksman", "2", 2, 4))
            dataRatings.add(Data_Rating("Assasins", "5", 5, 8))
            dataRatings.add(Data_Rating("Fighter", "10", 10, 23))
            return dataRatings
        }
}