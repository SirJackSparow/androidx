package com.example.androidjava.recyclerview_diffutils

import androidx.recyclerview.widget.DiffUtil

class RatingDiffCallback(private val oldList :  List<Data_Rating>, private val newList:List<Data_Rating>) :  DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].ratingValue == newList[newItemPosition].ratingValue
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val (_, _, name) = oldList[oldItemPosition]
        val (_, _, name1) = newList[newItemPosition]

        return name == name1
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }
}