package com.example.androidjava.navigationjetpack


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController

import com.example.androidjava.R
import kotlinx.android.synthetic.main.fragment_blank2.*

/**
 * A simple [Fragment] subclass.
 */
class BlankFragment : Fragment() {

    lateinit var viewmodelEx: ViewModelEx



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        viewmodelEx = ViewModelProviders.of(this).get(ViewModelEx::class.java)
        return inflater.inflate(R.layout.fragment_blank2, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        next.setOnClickListener { view ->

            //1 import by navigation findCintroller
           view?.findNavController()?.navigate(R.id.action_global_navigation2)

            //Navigation.findNavController(requireActivity())

            //import by navigation fragmentController
            // findNavController().navigate(R.id.action_blankFragment_to_blankFragment2)
        }
    }
}

