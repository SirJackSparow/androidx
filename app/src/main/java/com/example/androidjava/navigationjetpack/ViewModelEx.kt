package com.example.androidjava.navigationjetpack

import android.util.Log
import androidx.lifecycle.ViewModel

class ViewModelEx : ViewModel() {

    init {
        Log.i("TAG","ViewModelEx")
    }

    override fun onCleared() {
        super.onCleared()
    }
}