package com.example.androidjava.serviceintent

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidjava.R

const val TYPEONE = 1
const val TYPETWO = 2

class IntentService : AppCompatActivity() {

    //contoh intent service
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_service)

        val intent = Intent(this,MyIntentService::class.java)
        intent.putExtra("type", TYPEONE)
        startService(intent)

        val intent2 = Intent(this,MyIntentService::class.java)
        intent2.putExtra("type", TYPETWO)
        startService(intent2)

    }
}
