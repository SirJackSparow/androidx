package com.example.androidjava.serviceintent

import android.app.IntentService
import android.content.Intent
import android.util.Log


class MyIntentService : IntentService("MyIntentService") {


    //foreground service example palaying music
    //background service database operatiion , notification
    //bound service
    // contoh digunakan untuk download, playng music

    // sekarang kita contoh intent service, meskipun aplikasi di close tetap dijalankan codenya jka di intent

    override fun onHandleIntent(intent: Intent?) {

        when (intent?.getIntExtra("type", 0)) {

            TYPEONE -> {
                for (x in 1..5) {
                    Log.e("MyIntentService", "do something $x $TYPEONE")
                    Thread.sleep(1000)
                }
            }
            TYPETWO -> {
                for (x in 1..5) {
                    Log.e("MyIntentService", "do something $x $TYPETWO")
                    Thread.sleep(1000)
                }
            }

        }

    }

}
