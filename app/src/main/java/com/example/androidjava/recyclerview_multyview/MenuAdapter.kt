package com.example.androidjava.recyclerview_multyview

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidjava.R

class MenuAdapter(private val data: List<Any>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var datake: datake

    companion object {

        private lateinit var addedListener: ((MenuItem, Int) -> Unit)
        private lateinit var removedListener: ((MenuItem, Int) -> Unit)

        private const val ITEM_HEADER = 0
        private const val ITEM_MENU = 1
    }


    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            //model data type
            is String -> ITEM_HEADER
            // model data type
            is MenuItem -> ITEM_MENU
            else -> throw IllegalArgumentException("Undefined view type")
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_HEADER -> MenuHeaderHolder(parent.inflate(R.layout.item_header))
            ITEM_MENU -> MenuItemHolder(parent.inflate(R.layout.item_menu))
            else -> throw throw IllegalArgumentException("Undefined view type")
        }
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            ITEM_HEADER -> {
                val headerHolder = holder as MenuHeaderHolder
                headerHolder.bindContent(data[position] as String)
            }
            ITEM_MENU -> {
                val itemHolder = holder as MenuItemHolder
                itemHolder.bindContent(data[position] as MenuItem)
            }
            else -> throw IllegalArgumentException("Undefined view type")
        }

        holder.itemView.setOnClickListener {
            datake.onDataKe(position)
        }
    }

    fun OnClickDataKe(dataKe: datake) {
        datake = dataKe
    }


}

class MenuHeaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val itemHeader = itemView.findViewById(R.id.tvHeaderItem) as TextView

    fun bindContent(text: String) {
        itemHeader.text = text
    }
}

class MenuItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var jumlah: Int = 0
    private val itemName = itemView.findViewById(R.id.itemName) as TextView
    private val itemPrice = itemView.findViewById(R.id.itemPrice) as TextView
    private val itemCount = itemView.findViewById(R.id.itemCount) as TextView
    private val itemAdd = itemView.findViewById(R.id.itemAdd) as Button
    private val itemRemove = itemView.findViewById(R.id.itemRemove) as Button

    fun bindContent(menuItem: MenuItem) {
        itemCount.text = jumlah.toString()
        itemName.text = menuItem.name
        itemPrice.text = "Rp. ${menuItem.price}"


    }
}

interface datake {
    fun onDataKe(position: Int)
}
