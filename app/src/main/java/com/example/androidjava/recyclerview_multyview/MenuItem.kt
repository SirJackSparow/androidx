package com.example.androidjava.recyclerview_multyview

data class MenuItem(val name: String, val price: Int, var count: Int, val itemPreview: Int)