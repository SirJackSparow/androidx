package com.example.androidjava.recyclerview_multyview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidjava.R
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {

    lateinit var adapters: MenuAdapter

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        adapters = MenuAdapter(Menu.menus.toMutableList())

        adapters.OnClickDataKe(object : datake {
            override fun onDataKe(position: Int) {
                Toast.makeText(this@Main2Activity, position.toString(), Toast.LENGTH_SHORT).show()
            }

        })

        menuItem.apply {
            hasFixedSize()
            layoutManager = LinearLayoutManager(this.context)
            adapter = adapters
        }

    }
}
