package com.example.androidjava.rxjava

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.androidjava.R
import com.example.androidjava.rxjava.JokeApiServiceFactory.createService
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_rxmain.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

class RXMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rxmain)
    }


    @SuppressLint("CheckResult")
    fun helloWorldRxJava() {

        //untuk membuat instance menggunakan just, disini contoh single hanya memancarkan 1
        val observevable = Single.just("Hello World !!")

        observevable.subscribeBy(
                onSuccess = { Log.e("RxJava", it) },
                onError = {}
        )
    }

    @SuppressLint("CheckResult")
    fun rxDanmemperbaharuiThreadUI() {
        //untuk memperbaharui thread utama ketika memancarkan

        val observable = Single.just("Hello World !")

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onSuccess = { Log.e("RxJava", it) },
                        onError = {}
                )
    }

    //rx ini hanya memberitahu tidak memancarkan
    @SuppressLint("CheckResult")
    fun completable() {
        Completable.timer(3, TimeUnit.SECONDS)
                .subscribeBy(
                        onComplete = { Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show() },
                        onError = { Log.e("Rx", "Error") }
                )
    }

    @SuppressLint("CheckResult")
    //Metode doOnSubscribe dan doFinally dapat digunakan untuk memanggil suatu tindakan
    // tertentu sebelum subscription dan setelah onSuccess atau onError.

    fun callApiRxjava(aksi: Action) {
        val apiService = createService()

        val jokeObservable: Single<Joke> = apiService.randomJoke().map { it.joke }
        jokeObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { aksi.show() }
                .doFinally { aksi.hide() }
                .subscribeBy(
                        onSuccess = { print(it.jokeId) },
                        onError = { print(it) }
                )

    }
}

interface JokeApiService {
    @GET("jokes/random")
    fun randomJoke(): Single<JokeApiResponse>
}

data class JokeApiResponse(
        @SerializedName("type") val type: String,
        @SerializedName("value") val joke: Joke
)

data class Joke(
        @SerializedName("id") val jokeId: Int,
        @SerializedName("joke") val jokeText: String
)

interface Action {
    fun show()
    fun hide()
}

object JokeApiServiceFactory {
    fun createService(): JokeApiService = Retrofit.Builder()
            .baseUrl("http://api.icndb.com/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(JokeApiService::class.java)
}