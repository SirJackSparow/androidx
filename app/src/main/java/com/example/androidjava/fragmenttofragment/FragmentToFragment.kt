package com.example.androidjava.fragmenttofragment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidjava.R

class FragmentToFragment : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_to_fragment)
    }
}
