package com.example.androidjava.swipedeleteKotlin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import com.example.androidjava.R
import com.onesignal.OneSignal
import kotlinx.android.synthetic.main.activity_swipe_recycler_view.*

class SwipeRecyclerView : AppCompatActivity() {

    lateinit var Adapter :  Adapters
    val list : MutableList<ModelSwipeRecycler> = mutableListOf()
    lateinit var linearlayout : androidx.recyclerview.widget.LinearLayoutManager
    lateinit var  SwipeHelpers : SwipeHelper
    lateinit var itemTouchHelper: ItemTouchHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_swipe_recycler_view)

        OneSignal.startInit(this).inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()

        list.add(ModelSwipeRecycler("Naruto","action"))
        list.add(ModelSwipeRecycler("Sasuke","action"))
        list.add(ModelSwipeRecycler("Goku","adventure"))
        list.add(ModelSwipeRecycler("Sun Gokong","adventure"))
        list.add(ModelSwipeRecycler("echi","hot"))

        SwipeHelpers = SwipeHelper(object : SwipeControllerActions(){
            override fun onLeftClicked(position: Int) {
                super.onLeftClicked(position)
            }

            override fun onRightClicked(position: Int) {
                super.onRightClicked(position)
            }
        })

        Adapter = Adapters(list){}

        Adapter.notifyDataSetChanged()

        recycler.adapter = Adapter
        linearlayout = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.layoutManager = linearlayout
        itemTouchHelper = ItemTouchHelper(SwipeHelpers)
        itemTouchHelper.attachToRecyclerView(recycler)

    }
}
