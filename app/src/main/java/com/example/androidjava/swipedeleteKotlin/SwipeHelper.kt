package com.example.androidjava.swipedeleteKotlin


import android.annotation.SuppressLint
import android.graphics.Canvas
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.*
import android.view.MotionEvent
import android.view.View

class SwipeHelper(buttonActions: SwipeControllerActions) : ItemTouchHelper.Callback() {

    var swipeback: Boolean = false
    private var buttonShowedState = ButtonState.GONE
    private val buttonWidth = 300f

    override fun getMovementFlags(p0: androidx.recyclerview.widget.RecyclerView, p1: androidx.recyclerview.widget.RecyclerView.ViewHolder): Int {

        //bisa sweep kenama aja ,jika  kiri saja
        //return makeMovementFlags(0, LEFT)

        //jika semua bisa
        return makeMovementFlags(0, LEFT or RIGHT)
    }

    override fun onMove(p0: androidx.recyclerview.widget.RecyclerView, p1: androidx.recyclerview.widget.RecyclerView.ViewHolder, p2: androidx.recyclerview.widget.RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun onSwiped(p0: androidx.recyclerview.widget.RecyclerView.ViewHolder, p1: Int) {

    }

    override fun onChildDraw(c: Canvas, recyclerView: androidx.recyclerview.widget.RecyclerView, viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder,
                             dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        //handle swipe ketika digeser tidak hilang
        if (actionState == ACTION_STATE_SWIPE) {
            setTouchListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

    }

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {

        if (swipeback) {
            swipeback = buttonShowedState != ButtonState.GONE
            return 0
        }
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }


    @SuppressLint("ClickableViewAccessibility")
    fun setTouchListener(c: Canvas, recyclerView: androidx.recyclerview.widget.RecyclerView,
                         viewHoldel: androidx.recyclerview.widget.RecyclerView.ViewHolder, dx: Float,
                         dy: Float, actionState: Int, isCurrentlyActive: Boolean) {

        recyclerView.setOnTouchListener(View.OnTouchListener { v, event ->

            swipeback = event.action == MotionEvent.ACTION_CANCEL || event.action == MotionEvent.ACTION_UP

            if (swipeback) {

//                if (dx < -buttonWidth) buttonShowedState = ButtonState.RIGHT_VISIBLE
//                else if (dx > buttonWidth) buttonShowedState = ButtonState.LEFT_VISIBLE

                if (buttonShowedState != ButtonState.GONE) {
                    //  setTouchDownListener()
                    setItemClickable()
                }
            }
            return@OnTouchListener false
        })
    }

    private fun setItemClickable() {

    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchDownListener(c: Canvas, recyclerView: androidx.recyclerview.widget.RecyclerView, viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder,
                                     dx: Float, dy: Float, actionState: Int, isCurrentlyActive: Boolean) {

        recyclerView.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                setTopUpListener()
            }
            return@setOnTouchListener false
        }

    }

    private fun setTopUpListener() {

    }


    enum class ButtonState {
        GONE,
        LEFT_VISIBLE,
        RIGHT_VISIBLE
    }


}