package com.example.androidjava.swipedeleteKotlin;

public abstract class SwipeControllerActions {

    public void onLeftClicked(int position) {}

    public void onRightClicked(int position) {}

}
