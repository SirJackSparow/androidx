package com.example.androidjava.swipedeleteKotlin

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.androidjava.R

class Adapters(private val data: MutableList<ModelSwipeRecycler>, private val listener: (ModelSwipeRecycler) -> Unit)
    : androidx.recyclerview.widget.RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.itemadapterdeletecycle, p0, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItem(data[p1], listener)
    }

}

class ViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    val nama: TextView = view.findViewById(R.id.name)
    val age: TextView = view.findViewById(R.id.age)

    fun bindItem(data: ModelSwipeRecycler, listener: (ModelSwipeRecycler) -> Unit) {

        nama.text = data.nama
        age.text = data.age

        itemView.setOnClickListener {
            listener(data)
        }
    }
}